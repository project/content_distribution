-----------------------------------------
content_distribution configuration notes
-----------------------------------------

Typical Setup
	- 1 central content distribution site
	- 1-many remote content retriever sites

You should choose which drupal instance is going to be your 'content serving' instance.
This should be configured with the content_distribution module installed on it (see content distribution settings below)

You may then have 1 or more remote 'content retrieving' sites which will receive content
via web services from your 'content serving' drupal instance. All sites may sit on the same server or may be disparate
since all the data is sent and received via XMLRPC and the various services modules it does not matter.

To configure the content_distribution module you should do the following -

---------------------------------------------------------
Content distribution settings - distributor site (content serving) settings
---------------------------------------------------------
 - Install content_distribution module and relevant dependencies then:

 - admin/settings/distribution - set up website URLs of your 'content retrieving' sites in the form http://www.site.com

 - admin/build/services/settings - For best results Enable Session IDs for services, Enable Use Keys for services

 - admin/build/services/keys - Set up individual API keys for each remote website that you have

 - admin/user/user - set up a webservice user with a role with access to all permissions below

 - admin/user/permissions - configure the following permissions to be accessed by your webservice user

    - distributor service module permissions
	- file service module permissions
	- node service module permissions
	- nodequeue_service module permissions
	- services module permissions
	- system_service module permissions

 - admin/content/node-type/type - For each content type you want to distribute you should
 tick the box to 'enable node type to be distributed' under the content distribution settings fieldset.

 - Set up Nodequeues in admin/content/nodequeue
 - nodequeues allow you to filter / target content to specific sites. Each remote site can be configured to receive (or not) content from the
 available nodequeues set-up on the 'content serving' site. For example site1.com might be receiving content from nodequeue 1 and nodequeue 2 whereas
 site2.com might only receive content from nodequeue 2.
 - You will need to add at least 1 queue. A typical example is you might want to set-up a queue for each remote site you have.
 This will allow content to be targetted to each of these sites individual. You might also want to have a queue which gets received by 'all' sites (for general content for example).

-----------------------
Good test at this stage
-----------------------
 - try create a piece of distributable content on a node/add page - there should be 2 additional fieldsets on the node/add form
 - Published Date and Nodequeues for you to choose from to publish content to.
 
--------------------
Test view is working
--------------------
 - another good test at this stage is to test your content distribution view is working correctly. since this is what your remote
 site will interrogate to bring content over.
 
 - in admin/build/views/edit/content_distribution_queue - check view is configured correctly to display data - try previewing the view 
 it will accept arguments in the format - 
 
   - NodequeueID/ContentType/Date
   - Date should be in the format - YYYYMMDD
   - e.g. 1/news_item/20090101

----------------------------------------------
Content Retriever - remote site configurations
----------------------------------------------
 - install content_retriever module and relevant dependencies then:

 - *NOTE* the same content type must be installed on the distributing / remote site otherwise you will not be able to retrieve and save the nodes.

 - admin/build/services/settings - Enable use of sessid.

 - admin/settings/retriever - 

    - service endpoint (distribution site (content serving) xmlrpc url) - usually http://sitename.com/services/xmlrpc
	- Service API key for this site (as set up in distribution settings)
	- service username and password (as set up in distribution settings)
	- view to query on distribution site (by default this will be the view provided with the module 
	which has the name 'content_distribution_queue'
	- reset time retriever last run - Useful if you want to set the time back to re-import a bunch of nodes


 - admin/settings/retriever/types -
    - this page should show a list of 'available' content types to retrieve from the content serving site
    - if the remote site does not have one of the 'available' content types installed the module will: 
	  - ask if you want to install any content types which have not been installed on this site
	
	- skipping/installing then allows you to check which content types to retrieve from distribution server


 - admin/settings/retriever/queues 
    - This page shows a list of available nodequeue on the distribution site to retrieve content from.
    - check which queues you want your remote site to retrieve from.
    
 - admin/settings/retriever/output - this just runs the content retriever and shows a visual output of the nodes it retrieved.
useful for debugging purposes.
