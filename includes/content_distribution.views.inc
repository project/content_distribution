<?php 

/**
 * @file content_distribution.views.inc
 * Provides support for the Views module.
 *
 */

function content_distribution_views_default_views() {
  // get list of all available queue ids to populate 'relationship'
  // field of the view
  $qids = nodequeue_get_all_qids(0,0);
    
  $views = array();
    
  $view = new view;
$view->name = 'content_distribution_queue';
$view->description = 'Display a list of all nodes in queue \'Content Distribution Queue\'';
$view->tag = 'nodequeue';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('relationships', array(
  'nodequeue_rel' => array(
    'label' => 'queue',
    'required' => 1,
    'limit' => 1,
    'qids' => $qids,
    'id' => 'nodequeue_rel',
    'table' => 'node',
    'field' => 'nodequeue_rel',
    'relationship' => 'none',
  ),
));
$handler->override_option('fields', array(
  'title' => array(
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'label' => '',
    'relationship' => 'none',
    'link_to_node' => 1,
  ),
));
$handler->override_option('sorts', array(
  'position' => array(
    'id' => 'position',
    'table' => 'nodequeue_nodes',
    'field' => 'position',
    'order' => 'ASC',
    'relationship' => 'nodequeue_rel',
  ),
));
$handler->override_option('arguments', array(
  'qid' => array(
    'default_action' => 'not found',
    'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => 'All',
    'title' => '',
    'default_argument_type' => 'fixed',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'break_phrase' => 0,
    'not' => 0,
    'id' => 'qid',
    'table' => 'nodequeue_nodes',
    'field' => 'qid',
    'relationship' => 'nodequeue_rel',
    'default_options_div_prefix' => '',
    'default_argument_user' => 0,
    'default_argument_fixed' => '',
    'default_argument_php' => '',
    'validate_argument_node_type' => array(),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(),
    'validate_argument_type' => 'tid',
    'validate_argument_php' => '',
  ),
  'type' => array(
    'default_action' => 'not found',
    'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => 'All',
    'title' => '',
    'default_argument_type' => 'fixed',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'id' => 'type',
    'table' => 'node',
    'field' => 'type',
    'relationship' => 'none',
    'default_options_div_prefix' => '',
    'default_argument_user' => 0,
    'default_argument_fixed' => '',
    'default_argument_php' => '',
    'validate_argument_node_type' => array(),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(),
    'validate_argument_type' => 'tid',
    'validate_argument_php' => '',
  ),
  'changed' => array(
    'id' => 'changed',
    'table' => 'node',
    'field' => 'changed',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'default_argument_type' => 'php',
    'relationship' => 'none',
    'title' => '',
    'default_action' => 'not found',
    'wildcard' => '',
    'wildcard_substitution' => '',
    'default_options_div_prefix' => '',
    'default_argument_user' => 0,
    'default_argument_fixed' => '',
    'default_argument_php' => '',
    'validate_argument_node_type' => array(),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(),
    'validate_argument_type' => 'tid',
    'validate_argument_php' => '',
  ),
));
$handler->override_option('filters', array(
  'published_date' => array(
    'operator' => '<=',
    'value' => array(
      'type' => 'offset',
      'value' => 'now',
      'min' => '',
      'max' => '',
    ),
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => 'published_date_op',
      'label' => 'Content Distribution: Published Date',
      'use_operator' => 0,
      'identifier' => 'published_date',
      'optional' => 1,
      'remember' => 0,
    ),
    'id' => 'published_date',
    'table' => 'content_distribution',
    'field' => 'published_date',
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
  'role' => array(),
  'perm' => '',
));
$handler->override_option('title', 'Queue \'Content Distribution Queue\'');
$handler->override_option('use_pager', '1');
$handler->override_option('use_more', 1);
$handler->override_option('row_plugin', 'node');
$handler->override_option('row_options', array(
  'teaser' => 1,
  'links' => 1,
));
$handler = $view->new_display('page', 'Page', 'page');
$handler->override_option('path', 'nodequeue/distribution');
$handler->override_option('menu', array(
  'type' => 'none',
  'title' => '',
  'weight' => 0,
  'name' => 'navigation',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'weight' => 0,
));
$handler = $view->new_display('block', 'Block', 'block');
$handler->override_option('items_per_page', 5);
$handler->override_option('style_plugin', 'list');
$handler->override_option('style_options', array(
  'type' => 'ul',
));
$handler->override_option('row_options', array(
  'inline' => array(),
  'separator' => '',
));
$handler->override_option('block_description', 'Queue \'Content Distribution Queue\'');
$handler->override_option('block_caching', -1);


     
  
  $views[$view->name] = $view;
  return $views;
}

/**
 * Implementation of hook_views_data_alter()
 * 
 * Replaces the node.changed data for the views module and adds an
 * 'Updated date' argument.
 */
function content_distribution_views_data_alter(&$data) {
  // changed field
  $data['node']['changed'] = array(
    'title' => t('Updated date'), // The item it appears as on the UI,
    'help' => t('The date the node was last updated.'), // The help that appears on the UI,
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'field' => 'changed',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );  
}

/**
 * Define extra filters for the views module.
 */
function content_distribution_views_data(){
  $data = array();
	
  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['content_distribution']['table']['group']  = t('Content Distribution');

  $data['content_distribution']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Content Distribution'),
    'help' => t("Content distribution relates to nodes which are set up to be distributed remotely."),
  );
  
  $data['content_distribution']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );	
  // ----------------------------------------------------------------
  // Fields
  
  //published date
  $data['content_distribution']['published_date'] = array(
    'title' => t('Published Date'),
    'help' => t('Date and time of when node is set to be published.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  //week argument
  $data['content_distribution']['published_week'] = array(
    'title' => t('Published week'),
    'help' => t('In the form of WW (01 - 53).'),
    'argument' => array(
      'field' => 'published_date',
      'handler' => 'views_handler_argument_node_created_week',
    ),
  );
  return $data;
}


/**
 * Implementation of hook_views_query_alter()
 * 
 * Rewrites the where clause for the third argument (updated date)
 * so it is 'greater than or equals' rather than the default 'equals'
 * for the nodequeue view to function correctly
 */
function content_distribution_views_query_alter(&$view, &$query) {
  if ($view->name == 'content_distribution_queue') {
    $query->where[0]['clauses'][3] = "DATE_FORMAT((FROM_UNIXTIME(node.changed)), '%Y%m%%d%H%i') >= '%s'";
  }   
}

/**
 * Implementation of hook_views_api().
 */
function content_distribution_views_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'content_distribution') . '/includes',
  );
}