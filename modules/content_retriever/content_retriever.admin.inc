<?php
/**
 * form containing available admin settings
 * 
 * @return
 * array containing the form
 */
function content_retriever_admin_form() {
  //build the checkbox fields from the current local content types
  if (variable_get('drupal_http_request_fails', FALSE) == TRUE) {
    drupal_set_message(t('Drupal is unable to make HTTP requests. Please reset the HTTP request status.'), 'error', FALSE);
    watchdog('integration', t('Drupal is unable to make HTTP requests. Please reset the HTTP request status.'), array(), WATCHDOG_CRITICAL);
  }
  $last_run = variable_get('content_retriever_last_run',format_date(time(), 'custom', 'Y-m-d H:i'));
  $last_run = format_date($last_run, 'custom', 'Y-m-d H:i');
  $form = array();
  $form['endpoint'] = array(
	'#title' => t('Web service endpoint'),
	'#type' => 'textfield',
	'#required' => 'true',
	'#default_value' => variable_get('content_retriever_endpoint', 'http://cms.defaqto.com/services/xmlrpc'),
  );
  $form['api_key'] = array(
	'#title' => t('Web service API key'),
	'#type' => 'textfield',
	'#default_value' => variable_get('content_retriever_api_key', ''),
  );
  $form['username'] = array(
	'#title' => t('Web service User name'),
	'#type' => 'textfield',
	'#default_value' => variable_get('content_retriever_webservice_username', ''),
  );
  $form['password'] = array(
	'#title' => t('Web service Password'),
	'#type' => 'textfield',
	'#default_value' => variable_get('content_retriever_webservice_pass', ''),
  );
  $form['view'] = array(
    '#title' => t('Name of the view to query on the distribution server'),
    '#type' => 'textfield',
    '#required' => 'true',
    '#default_value' => variable_get('content_retriever_viewname', 'content_distribution_queue'),
  );  
  $form['reset_timestamp'] = array(
    '#type' => 'textfield',
    '#title' => t('Reset the time the retriever was last run'),
    '#size' => 40,
    '#default_value' => $last_run,
    '#description' => t('Use the Date Format: YYYY-MM-DD HH:MM. This will only be reset if you click \'Reset last run time\''),
  );
  $form['reporting'] = array(
    '#type' => 'checkbox',
    '#title' => 'On screen reporting messages',
    '#default_value' => variable_get('content_retriever_screen_messages', 0),
  ); 
  $form['submit'] = array(
    '#value' => 'Save settings',
	'#type' => 'submit',
  );
  $form['retrieve'] = array(
    '#value' => 'Retrieve content now',
    '#type' => 'submit',
    '#name' => 'retrieve',
    '#submit' => array('content_retriever_admin_retrieve_now_form_submit'),
  );
  $form['reset_status'] = array(
	'#value' => 'Reset HTTP request status',
	'#type' => 'submit',
	'#name' => 'reset_status',
	'#submit' => array('content_retriever_admin_clear_http_status_form_submit'),
  );
  
  return $form; 
}

/**
 * content types form.
 */
function content_retriever_admin_types_form($form_values = null) {
  
  // retrieve array of content types for distribution from web service
    $endpoint = variable_get('content_retriever_endpoint', 'http://cms.defaqto.com/services/xmlrpc');
    $api_key = variable_get('content_retriever_api_key', '');
    
    // connect to the system service to get a session id
    $connect = content_retriever_xmlrpc($endpoint, 'system.connect', $api_key);
    $session_id = $connect['sessid'];
    
    //retrieve content types   
    $fields  = content_retriever_xmlrpc($endpoint, 'node.getAllTypes', $api_key, $session_id);
    
    // get local content types installed.
    $local_types = array();
    $local_types = array_keys(node_get_types());
    
    if(!empty($fields)){
      $not_installed = array_diff($fields, $local_types);
    
      // get list of installed types
      foreach($not_installed as $item){
        unset($fields[$item]);
      }
      
      //variable_set('content_retriever_content_types', $fields);
    }else{
    	drupal_set_message(t('Could not retrieve a list of content types from the remote server'), 'error');
    }
    
    // check which state of the form you are in
    // and render form depending on if you are selecting content 
    // types or installing content types 
    if((!empty($not_installed)) && (empty($form_values['skip_install']))){
      //define our admin form
      
      if(empty($fields)){
      	drupal_set_message(t('There are no content types currently installed which can be retrieved from the remote instance.'), 'error');
      }      
	  $form = array();
	  $default_value = array_values($not_installed);
	  $form['content_type_information'] = array(
        '#value' => variable_get('content_type_information', t('Below is a list of content types set up for distribution by the remote instance but which are <strong>not</strong> ' .
      	'installed on this instance. If you do not wish to install the selected content type <strong>click skip</strong> until you are presented with the <em>content types to retrieve form</em>.')),
      );
	  $form['not_installed'] = array(
	    '#title' => 'These content types are not installed do you want to install any now',
	    '#type' => 'radios',
	    '#options' => $not_installed,
	    '#default_value' => $default_value[0],
	  );
	  $form['skip'] = array(
	    '#value' => 'Skip',
	    '#type' => 'submit',
	    '#name' => 'skip',
	    '#submit' => array('content_retriever_admin_skip_form_submit'),
	  );
	  $form['install'] = array(
	    '#value' => 'Install',
	    '#type' => 'submit',
	    '#name' => 'install',
	    '#submit' => array('content_retriever_admin_install_types_form_submit'),
	  );
	  $form['reset_status'] = array(
	    '#value' => 'Reset HTTP request status',
	    '#type' => 'submit',
	    '#name' => 'reset_status',
	    '#submit' => array('content_retriever_admin_clear_http_status_form_submit'),
	  );
    }else{ 
      $form = array();
      if(!empty($fields)){
        $form['content_type_information'] = array(
          '#value' => variable_get('content_type_retrieve_information', t('Select which content types you would like to retrieve from the remote instance.')), 	  
        );
        $form['content_types'] = array(
	      '#title' => 'Content types to retrieve',
	      '#type' => 'checkboxes',
	      '#options' => $fields,
	      '#default_value' => variable_get('content_retriever_content_types', array('page', 'story')),
	    );	  
	    $form['submit'] = array(
	      '#value' => 'Save settings',
	      '#type' => 'submit',
	    );
	    $form['retrieve'] = array(
          '#value' => 'Retrieve content now',
          '#type' => 'submit',
          '#name' => 'retrieve',
          '#submit' => array('content_retriever_admin_retrieve_now_form_submit'),
        );
	    $form['reset_status'] = array(
	      '#value' => 'Reset HTTP request status',
	      '#type' => 'submit',
	      '#name' => 'reset_status',
	      '#submit' => array('content_retriever_admin_clear_http_status_form_submit'),
	    );
      }else{
      	$form['content_type_information'] = array(
          '#value' => variable_get('no_types_installed_information', t('<p>You may need to configure the content distribution module on the remote instance to allow content to be distributed.</p><p>Or you can try resetting the HTTP request status.</p>')), 	  
        );
        $form['reset_status'] = array(
	      '#value' => 'Reset HTTP request status',
	      '#type' => 'submit',
	      '#name' => 'reset_status',
	      '#submit' => array('content_retriever_admin_clear_http_status_form_submit'),
	    );
      }
      
    }
        
    return $form;   
}
 
/**
 * submit function fires the node retrieval function
 */
function content_retriever_admin_retrieve_now_form_submit($form_id, $form_values) {
  $last_run = variable_get('content_retriever_last_run', (time() - (24 * 60 * 60)) );
  if(_content_retriever_save_nodes($last_run)){
    drupal_set_message(t('Process complete.'));
    variable_set('content_retriever_last_run', time());
  }
}
/**
 * Submit function to skip the content type install process.
 */
function content_retriever_admin_skip_form_submit($form, &$form_state) {
	// rebuild the form but go to selection of types to retrieve
	// instead of installing
	$form_state['rebuild'] = TRUE;
	$form_state['skip_install'] = true;
}


/**
 * Submit function to install selected content type.
 */
function content_retriever_admin_install_types_form_submit($form, &$form_state) {
  $install_type = $form_state['values']['not_installed'];
  $query = 'destination=admin/settings/retriever/types' . '&type='.$install_type;	  
  // if content type installer is available use it to install content types
  if (module_exists('defaqto_content_type_installer')) {
    $types_available = _defaqto_content_type_installer_get_files('content');
 
    // see if content type .inc file is available to install
    foreach($types_available as $type){
      if(array_search($install_type, $type) !== false){
        $form_state['values']['content_types'] = array($install_type);
        $form_state['values']['taxonomy'] = array();
        defaqto_content_type_installer_admin_form_submit(&$form, &$form_state);
        $installed = true; 	
      }
    }
    if(!$installed){
      // include does not exist so go to content type add form.
      drupal_goto('admin/content/types/add', $query);
    }	 
  }
  else{
  	// go to content type add form.
  	drupal_goto('admin/content/types/add', $query);
  }
}

/**
 * submit function sets the HTTP request variable back to FALSE if it has
 * inadvertently been made TRUE by a faulty web service/bad address
 */
function content_retriever_admin_clear_http_status_form_submit($form_id, $form_state) {
  variable_set('drupal_http_request_fails', FALSE);
  drupal_set_message(t('HTTP request status reset.'));
}

/**
 * validate function to check user-entered data from the admin form
 */
function content_retriever_admin_form_validate($form, &$form_state) {
  if (strpos($form_state['values']['endpoint'], 'http') != 0) {
    form_set_error('endpoint', t('The service endpoint must be a valid HTTP or HTTPS web address.'));
  }
  $reset = $form_state['values']['reset_timestamp'];
  // get the current timestamp and convert user entered date to
  // timestamp to compare
  $now = time();
  $reset_timestamp = date_convert($reset,DATE_DATETIME, DATE_UNIX);
  
  // set form error if invalid date format
  if(!date_is_valid($reset, $type = DATE_DATETIME)){
  	form_set_error('reset_timestamp', t('Please enter a valid date format.'));
  }else if($reset_timestamp > $now){
  	form_set_error('reset_timestamp', t('Please enter a time in the past to reset the last run time to.'));
  }
  
}
/**
 * submit function for 'Settings' form.
 */
function content_retriever_admin_form_submit($form, &$form_state){
  // save api key and endpoint variables
  variable_set('content_retriever_endpoint', $form_state['values']['endpoint']);
  variable_set('content_retriever_api_key', $form_state['values']['api_key']);
  variable_set('content_retriever_viewname', $form_state['values']['view']);
  variable_set('content_retriever_webservice_username', $form_state['values']['username']);
  variable_set('content_retriever_webservice_pass', $form_state['values']['password']);
  variable_set('content_retriever_screen_messages', $form_state['values']['reporting']);
  
  $time = $form_state['values']['reset_timestamp'];
  // convert date to unix timestamp
  $reset = date_convert($time,DATE_DATETIME, DATE_UNIX);
  variable_set('content_retriever_last_run', $reset);
  
  drupal_set_message(t('Content Retriever Settings Saved.'));
}

/**
 * submit function for the 'Content Types' form sets variables for this module:
 * 
 * content_retriever_content_types 
 * an array of content types to be retrieved
 * 
 */
function content_retriever_admin_types_form_submit($form, &$form_state) {
  //organise the content types in to the correct format
  $selected_types = array();
  $selected_types = $form_state['values']['content_types'];
  $save_types = array();
  $count = 0;
  foreach ($selected_types as $selected_type) {
    if ($selected_type != '0') {
      $count++;
      $save_types[] = $selected_type;
    }
  }
  //inform user if the module is disabled by their content choices
  if ($count == 0) {
    drupal_set_message(t('No content types were selected. Module disabled.'), 'error');
  }
  
  //save the variables
  variable_set('content_retriever_content_types', $save_types);
  
  drupal_set_message(t('Settings saved.'));
  //since this is a critical module, tell watchdog settings have changed so we have an audit trail
  watchdog(t('integration'), t('Content Retriever Type settings were changed.'), array(), WATCHDOG_NOTICE);
  
  // rebuild the form and keep on the retrieval selection state
  $form_state['rebuild'] = TRUE;
  $form_state['skip_install'] = true;
}

/**
 * Form containing the settings for the nodequeues
 * 
 * @return $form
 * form array
 */
function content_retriever_admin_queues_form(){
  $form = array();
  
  //make sure we can actually make HTTP requests
  if (variable_get('drupal_http_request_fails', FALSE) == TRUE) {
    drupal_set_message(t('Drupal is unable to make HTTP requests. Please reset the HTTP request status.'), 'error', FALSE);
    watchdog('integration', t('Drupal is unable to make HTTP requests. Please reset the HTTP request status.'), array(), WATCHDOG_CRITICAL);
  } else {
    //get list of nodequeues from remote web service
    $api_key = variable_get('content_retriever_api_key', '');
    $endpoint = variable_get('content_retriever_endpoint', 'http://cms.defaqto.com/services/xmlrpc');
    
    // connect to the web service to get a session id
    $connect = content_retriever_xmlrpc($endpoint, 'system.connect', $api_key);
    $session_id = $connect['sessid'];

    $queues = content_retriever_xmlrpc($endpoint, 'nodequeue.getQueues', $api_key, $session_id);

    if ($queues) {
      foreach ($queues as $queue) {
        $queue_fields[$queue['qid']] = $queue['title'];
      }
    } else {
      drupal_set_message(t('No queues available.'));
    }
  }
  
  $form['queues'] = array(
    '#title' => 'Nodequeues to interrogate',
    '#type' => 'checkboxes',
    '#options' => $queue_fields,
    '#default_value' => variable_get('content_retriever_nodequeues', array()),
  );
  $form['submit'] = array(
    '#value' => 'Save settings',
    '#type' => 'submit',
  );
  $form['retrieve'] = array(
    '#value' => 'Retrieve content now',
    '#type' => 'submit',
    '#name' => 'retrieve',
    '#submit' => array('content_retriever_admin_retrieve_now_form_submit'),
  );
  $form['reset_status'] = array(
	'#value' => 'Reset HTTP request status',
	'#type' => 'submit',
	'#name' => 'reset_status',
	'#submit' => array('content_retriever_admin_clear_http_status_form_submit'),
  );
  
  return $form;
}
/**
 * Submit function for 'queues' form.
 */
function content_retriever_admin_queues_form_submit($form, &$form_state){
  //collate the nodequeues ready for saving
  $queues = array();
  $queues = $form_state['values']['queues'];
  $count = 0;
  foreach ($queues as $queue) {
    if ($queue != '0') {
      $count++;
    }
  }
  //inform user if the module is disabled by their nodequeue choices
  if ($count == 0) {
    drupal_set_message(t('No nodequeues were selected. Module disabled.'), 'error');
  }
  
  //save the variables
  variable_set('content_retriever_nodequeues', $queues);
  drupal_set_message(t('Nodequeue Settings Saved.'));
}

/**
 * theme function rendering the 'settings' admin page.
 */
function theme_content_retriever_admin_page() {
  $output .= '<p><span style="color:red; font-weight: bold">Important!</span> '.t('After a failed HTTP request (e.g. web service goes down, endpoint URL is wrong, etc.) Drupal assumes it has no access to the Internet and prevents itself from trying to make HTTP requests again. You may use the "Reset HTTP request status" button below to reset Drupal and allow it to make HTTP requests again. Always try this if you are not receiving data and you don\'t know why.').'</p>';
  $output .= drupal_get_form('content_retriever_admin_form');
  return $output;
}

/**
 * theme function for rendering the 'content types' admin page
 */
function theme_content_retriever_admin_types_page() {
 // $output .= '<p>' .t('Choose which content types are to be retrieved.').'</p>';
  $output .= drupal_get_form('content_retriever_admin_types_form');
  return $output;
}

/**
 * Theme function for rendering the 'queues' admin page
 */
function theme_content_retriever_admin_queues_page(){
  $output .= drupal_get_form('content_retriever_admin_queues_form');
  return $output;
}

