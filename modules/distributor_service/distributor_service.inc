<?php
/**
 * @author gtaylor
 * @file
 *  Link node distribution functionalities to services module.
 */


/**
 * Check if the user is allowed to access the node data.
 *
 */
function distributor_service_get_access() {
  global $user;
  if (user_access('get any node distribution data')) {
    return TRUE;
  }
}

/**
 * Check if the user is allowed to delete the node.
 *
 */
function distributor_service_delete_access() {
  global $user;
  if((user_access('delete distributed nodes'))){
  	return TRUE;
  }
}


/**
 * Returns a specified node.
 *
 * @param $type
 *   String. The node Type.
 * 
 * @return
 *   Boolean. The node is set up to be distributed or not.
 */
function distributor_service_get_type($type) {
  $is_distributable = variable_get('distribute_content_type_'.$type, 0);
  if($is_distributable !== 0){
    return true;	
  }else{
  	return false;
  }
  
}

/**
 * Determine whether all content types are set up 
 * for distribution.
 *
 * @return
 *   Array. Array of content types with a flag if
 *   their nodes are distributable or not.
 */
function distributor_service_get_all_types() {
  $content_types = node_get_types();
  foreach($content_types as $type){
  	$is_distributable = variable_get('distribute_content_type_'.$type->type,0);
  	if($is_distributable !== 0){
  		$types[$type->type] = $type->type;
  	}
  }
  return $types;  
}


/**
 * look's up the node id on the local server based on the node id which
 * is being deleted on the other site and delete's the corresponding node
 * 
 * @param $nid
 * the node id to look up
 */
function distributor_service_delete_node($nid){
  global $user;
  
  //we need to be a superuser to do this next stuff
  //this should be refactored so the webservice logs in
  //using the user service
  $temp_user = $user;
  $user = user_load(array('uid' => 1));
  
  // find the local node id.
  $result = db_query("SELECT nid FROM {content_retriever} WHERE original_nid = %d", $nid);
  $node_id = (int)db_result($result);
  
  //delete the node
  if($node_id)
    node_delete($node_id);
  
  $user = $temp_user;  
  return $node_id;
}



